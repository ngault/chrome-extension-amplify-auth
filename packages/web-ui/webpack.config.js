const webpack = require("webpack");
const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");

if (!process.env.NODE_ENV) {
  throw new Error(
    'webpack.config.js: The NODE_ENV environment variable is required but was not specified.'
  );
}

// Control by environment variables
const isEnvProduction = process.env.NODE_ENV === "production";
const shouldUseSourceMap = process.env.GENERATE_SOURCEMAP !== "false"

const config = {
  mode: isEnvProduction ? "production" : "development",
  entry: {
    index: path.join(__dirname, "src/index.tsx"),
  },
  output: { path: path.join(__dirname, "dist"), filename: "[name].js" },
  module: {
    rules: [
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false
        }
      },      
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
        exclude: /\.module\.css$/,
      },
      {
        test: /\.ts(x)?$/,
        loader: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              importLoaders: 1,
              modules: true,
            },
          },
        ],
        include: /\.module\.css$/,
      },
      {
        test: /\.svg$/,
        use: "file-loader",
      },
      {
        test: /\.png$/,
        use: [
          {
            loader: "url-loader",
            options: {
              mimetype: "image/png",
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx", ".tsx", ".ts"],
    alias: {
      "react-dom": "@hot-loader/react-dom",
    },
  },
  devServer: {
    static: {
      directory: path.join(__dirname, "dist"),
    },
    compress: true,
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: "public", to: "." }],
    })
  ],

  // You need to specify how to create source map since the default value "eval"
  // causes error due to content security policy of Chrome extension.
  // https://stackoverflow.com/questions/48047150/chrome-extension-compiled-by-webpack-throws-unsafe-eval-error
  devtool: isEnvProduction
  ? shouldUseSourceMap
    ? "source-map"
    : false
  : "cheap-module-source-map",
};

module.exports = config;

import { withAuthenticator } from '@aws-amplify/ui-react';
import { awsExports } from 'amplify-shared';
import Amplify, {Auth} from 'aws-amplify';
import {useCallback} from 'react';

import TodoView from './TodoView';

Amplify.configure(awsExports);

function App() {
  const handleSignOut = useCallback(async () => {
    try {
      await Auth.signOut();
    } catch(err) {
      console.log('Sign out fail. err=', err);
    }
  }, []);

  return (
    <div>
      <button onClick={handleSignOut}>Sign out</button>
      <TodoView/>
    </div>
  );
}

export default withAuthenticator(App);

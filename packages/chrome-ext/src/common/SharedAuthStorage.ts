import { Logger } from 'aws-amplify';

const CHROME_STORAGE_KEY_PREFIX = 'AmplifyStorage-';

/**
 * Use logger of Amplify.
 * https://docs.amplify.aws/lib/utilities/logger/q/platform/js/
 */
const logger = new Logger('SharedAuthStorage');

type OperatorType = (items: object) => void;

/**
 * Enumerate all relevant key-value items in chrome.storage.local.
 * @param operator - operator to apply on items
 */
function enumerateItems(operator: OperatorType) {
  chrome.storage.local.get(null, (items) => {
    const chromeStorageKeys = Object.keys(items).filter((key) => key.startsWith(CHROME_STORAGE_KEY_PREFIX));
    chrome.storage.local.get(chromeStorageKeys, (items => {
      // items is an object which has key-value.
      // Each key has a prefix, and you need to remove it if you want to access on-memory cache.
      operator(items);
    }));
  });
}

/**
 * When local storage is updated in Chrome extension popup (via sign in/out),
 * update on-memory cache.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
chrome.storage.onChanged.addListener((changes: any) => {
  const chromeStorageKeys = Object.keys(changes);
  for(const chromeStorageKey of chromeStorageKeys) {
    if (chromeStorageKey.startsWith(CHROME_STORAGE_KEY_PREFIX)) {
      const key = chromeStorageKey.replace(CHROME_STORAGE_KEY_PREFIX, '');
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const change: any = changes[chromeStorageKey];
      if (change?.oldValue) {
        SharedAuthStorage.cache.delete(key);
      } else if (change?.newValue) {
        SharedAuthStorage.cache.set(key, change?.newValue);
      }
    }
  }
});

export default class SharedAuthStorage {
  static syncPromise: Promise<void> | null = null;
  static cache = new Map();
  
  /**
   * This is used to set a specific item in storage
   */
  static setItem(key:string, value:string) {
    chrome.storage.local.set({[CHROME_STORAGE_KEY_PREFIX + key]: value});
    SharedAuthStorage.cache.set(key, value);

    logger.debug(`setItem(): key=${key}, value=${value}`);
  }

  /**
   * This is used to get a specific key from storage.
   * If the key is not found in on-memory cache, it returns null.
   * 
   * Note the chrome.storage.local may have that key. The caller is
   * supposed to wait for SharedAuthStorage.sync to be resolved.
   */
  static getItem(key:string) {
    let value = null;
    if (SharedAuthStorage.cache.has(key)) {
      value = SharedAuthStorage.cache.get(key);
    }
    
    logger.debug(`getItem(): key=${key}, value=${value}`);
    return value;
  }

  /**
   * This is used to remove an item from storage
   */
  static removeItem(key: string) {
    chrome.storage.local.remove(CHROME_STORAGE_KEY_PREFIX + key);
    SharedAuthStorage.cache.delete(key);

    logger.debug(`removeItem(): key=${key}`);
  }

  /**
   * This is used to clear the storage
   */
  static clear() {
    enumerateItems(items => {
      const keys = Object.keys(items);
      chrome.storage.local.remove(keys);

      logger.debug(`clear(): for chrome.storage.local.remove(), keys=${keys}`);
    });

    SharedAuthStorage.cache.clear();
  }

  /**
   * Will sync Auth data from chrome.storage.local to on-memory cache.
  */
  static sync() {
    if (!SharedAuthStorage.syncPromise) {
      SharedAuthStorage.syncPromise = new Promise<void>((res) => {
        enumerateItems(items => {
          for (const [chromeStorageKey, value] of Object.entries(items)) {
            const key = chromeStorageKey.replace(CHROME_STORAGE_KEY_PREFIX, '');
            SharedAuthStorage.cache.set(key, value);

            logger.debug(`sync(): key=${key}, value=${value}`);
          }
          res();
        });
      });
    }
    return SharedAuthStorage.syncPromise;
  }

  /**
   * Check if you are signed in.
   * @returns true if you are signed in.
   */
  static isSignedIn() : boolean {
    let signedIn = false;
    SharedAuthStorage.cache.forEach((value, key) => {
      if (key.endsWith('accessToken')) {
        signedIn = true;
      }
    })
    return signedIn;
  }

  /**
   * Dump key-value pairs for debug.
   */
  static dump() {
    enumerateItems(items => {
      for (const [chromeStorageKey, value] of Object.entries(items)) {
        const key = chromeStorageKey.replace(CHROME_STORAGE_KEY_PREFIX, '');
        console.log(`${key}: ${value}`);
      }
    });
  }
}

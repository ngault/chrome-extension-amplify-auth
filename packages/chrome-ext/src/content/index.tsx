import ReactDOM from 'react-dom';

import Overlay from './Overlay';

const Main = () => {
  return (
    <Overlay/>
  );
}

// Place the overlay window on top of the content.
const app = document.createElement('div');
document.body.appendChild(app);
app.style.display = 'flex';
ReactDOM.render(<Main />, app);

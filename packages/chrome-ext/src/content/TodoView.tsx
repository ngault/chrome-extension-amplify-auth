// This source code is basically the same as
// https://docs.amplify.aws/start/getting-started/data-model/q/integration/react/#connect-frontend-to-api

import { createTodo, listTodos } from 'amplify-shared';
import { API } from 'aws-amplify';
import { useEffect, useState } from 'react';

type Todo = {
  id?: string,
  name: string,
  description: string
};

const initialState = { name: '', description: '' };

const TodoView = () => {
  const [formState, setFormState] = useState(initialState);
  const [todos, setTodos] = useState<Todo[]>([]);

  useEffect(() => {
    fetchTodos();
  }, []);

  function setInput(key: string, value: string) {
    setFormState({ ...formState, [key]: value });
  }

  async function fetchTodos() {
    try {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const todoData: any = await API.graphql({
        query: listTodos,
        authMode: 'AMAZON_COGNITO_USER_POOLS'
      });
      const todos = todoData.data.listTodos.items;
      setTodos(todos);
    } catch (err) { console.log('error fetching todos. ', err); }
  }

  async function addTodo() {
    try {
      if (!formState.name || !formState.description) return;
      const todo = { ...formState };
      setTodos([...todos, todo]);
      setFormState(initialState);
      await API.graphql({
        query: createTodo, 
        variables: {input: todo},
        authMode: 'AMAZON_COGNITO_USER_POOLS'
      });
    } catch (err) {
      console.log('error creating todo:', err);
    }
  }

  return (
    <div style={styles.container}>
      <h2>Amplify Todos</h2>
      <input
        onChange={event => setInput('name', event.target.value)}
        style={styles.input}
        value={formState.name}
        placeholder="Name"
      />
      <input
        onChange={event => setInput('description', event.target.value)}
        style={styles.input}
        value={formState.description}
        placeholder="Description"
      />
      <button style={styles.button} onClick={addTodo}>Create Todo</button>
      {
        todos.map((todo, index) => (
          <div key={todo.id ? todo.id : index} style={styles.todo}>
            <p style={styles.todoName}>{todo.name}</p>
            <p style={styles.todoDescription}>{todo.description}</p>
          </div>
        ))
      }
    </div>
  )
}

const styles = {
  container: { width: 400, margin: '0 auto', display: 'flex', flexDirection: 'column' as const, justifyContent: 'center', padding: 20 },
  todo: {  marginBottom: 15 },
  input: { border: 'none', backgroundColor: '#ddd', marginBottom: 10, padding: 8, fontSize: 18 },
  todoName: { fontSize: 20, fontWeight: 'bold' },
  todoDescription: { marginBottom: 0 },
  button: { backgroundColor: 'black', color: 'white', outline: 'none', fontSize: 18, padding: '12px 0px' }
};

export default TodoView;

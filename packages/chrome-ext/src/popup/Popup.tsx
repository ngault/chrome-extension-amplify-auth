import { withAuthenticator } from '@aws-amplify/ui-react';
import { awsExports } from 'amplify-shared';
import Amplify, {Auth} from 'aws-amplify';
import {useCallback} from 'react';

import SharedAuthStorage from '../common/SharedAuthStorage';

Amplify.configure({
  ...awsExports,
  Auth: {
    storage: SharedAuthStorage
  }
});

const styles = {
  container: { 
    width: 400
  }
};

function Popup() {
  const handleSignOut = useCallback(async () => {
    try {
      await Auth.signOut();
    } catch(err) {
      console.log('Sign out fail. err=', err);
    }
  }, []);

  return (
    <div style={styles.container}>
      <button onClick={handleSignOut}>Sign out</button>
    </div>
  );
}

export default withAuthenticator(Popup);

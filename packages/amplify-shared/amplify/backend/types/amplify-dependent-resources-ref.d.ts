export type AmplifyDependentResourcesAttributes = {
    'auth': {
        'amplifyshared16c69fc8': {
            'IdentityPoolId': 'string',
            'IdentityPoolName': 'string',
            'UserPoolId': 'string',
            'UserPoolArn': 'string',
            'UserPoolName': 'string',
            'AppClientIDWeb': 'string',
            'AppClientID': 'string'
        }
    },
    'api': {
        'amplifyshared': {
            'GraphQLAPIKeyOutput': 'string',
            'GraphQLAPIIdOutput': 'string',
            'GraphQLAPIEndpointOutput': 'string'
        }
    }
}

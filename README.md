# chrome-extension-amplify-auth

This is a boilerplate for Chrome extension with amplify authentication.

For details, please read [this article](https://medium.com/@ken.miyasita/developing-chrome-extensions-with-amplify-authentication-be9b9e496a06)

![title](https://gitlab.com/kmiyashita/chrome-extension-amplify-auth/uploads/af390515518706632d9b998b70a1d906/title.png)